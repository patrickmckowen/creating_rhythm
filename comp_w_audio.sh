# The following solution reads from a file if the
# script is called with a file name as the first 
# parameter $1 otherwise from standard input.
while read line

  do
    declare -a arr
    # Create array from strings
    numbers=$(echo "$line" | tr " " "\n")

    ## now loop through the above array
    for i in $numbers
      do
        # These will be background processes ... ghetto
        play -n synth 0.1 tri  1000.0 &
        sleep 1 &

        play "prog/kick_1.wav"
        sleep "$i"
    done

  done < "${1:-/dev/stdin}"
